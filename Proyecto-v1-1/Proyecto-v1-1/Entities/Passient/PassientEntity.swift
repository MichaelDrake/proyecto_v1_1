//
//  PassientEntity.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/6/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//


import Foundation
import RealmSwift

class PassientEntity:Object{
    
    @objc dynamic var userId:String = ""
    @objc dynamic var name:String = ""
    @objc dynamic var birthday:String = ""
    @objc dynamic var gender:String = ""
    @objc dynamic var bloodType:String = ""
    @objc dynamic var allergies:String = ""
    @objc dynamic var hereditaryDiseases:String = ""
    
    
    convenience init(id:String,nam:String,brd:String,gen:String,blt:String, all:String, hdi:String){
        self.init()
        
        userId = id
        name = nam
        birthday = brd
        gender = gen
        bloodType = blt
        allergies = all
        hereditaryDiseases = hdi
        
    }
    
    
    
    override static func primaryKey() -> String?{
        return "userId"
    }
}

