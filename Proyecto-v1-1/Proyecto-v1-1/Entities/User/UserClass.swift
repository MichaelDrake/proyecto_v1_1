//
//  UserEntity.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 6/29/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import Foundation
import RealmSwift

class UserClass:Object{
    
    @objc dynamic var userId:String = ""
    @objc dynamic var email:String = ""
    @objc dynamic var password:String = ""
    @objc dynamic var type:String = ""
    
    
    convenience init(id:String,ml:String,ps:String,ty:String){
        self.init()
        
        userId = id
        email = ml
        password = ps
       type = ty
        
    }
   
    
    
    override static func primaryKey() -> String?{
        return "userId"
    }
}

