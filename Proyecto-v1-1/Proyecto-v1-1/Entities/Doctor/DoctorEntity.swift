//
//  DoctorEntity.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/6/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import Foundation

import RealmSwift

class DoctorEntity:Object{
    
    @objc dynamic var userId:String = ""
    @objc dynamic var name:String = ""
    @objc dynamic var birthday:String = ""
    @objc dynamic var gender:String = ""
    @objc dynamic var cellphone:String = ""
    @objc dynamic var addressLatitude:String = ""
    @objc dynamic var addressLongitude:String = ""
    @objc dynamic var especiality:String = ""
    @objc dynamic var university:String = ""
    
    
    
    convenience init(id:String,nam:String,birt:String,gen:String,cell:String, addrLat:String, addrLon:String,espe:String, univ:String){
        self.init()
        
        userId = id
         name = nam
        birthday = birt
        gender = gen
        cellphone = cell
         addressLatitude = addrLat
        addressLongitude = addrLon
         especiality = espe
         university = univ
        
    }
    
    
    
    override static func primaryKey() -> String?{
        return "userId"
    }
}


