//
//  AppointmentEntity.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/12/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import Foundation
import RealmSwift

class AppointmentClass:Object{
    
    @objc dynamic var appointmentId:Int = 0
    @objc dynamic var passientId:String = ""
    @objc dynamic var doctorId:String = ""
    @objc dynamic var symptoms:String =  ""
    @objc dynamic var observations:String = ""
    @objc dynamic var date:String = ""
    @objc dynamic var state:String = ""
    
    convenience init(appo:Int,pass:String,doct:String,symp:String,obse:String, dat:String, stat:String){
        self.init()
        appointmentId = appo
        passientId = pass
        doctorId = doct
        symptoms = symp
        observations = obse
        date = dat
        state = stat
    }
    override static func primaryKey() -> String?{
        return "appointmentId"
    }
}

