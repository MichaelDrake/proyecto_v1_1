//
//  PrescriptionEntity.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/20/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import Foundation
import RealmSwift

class PrescriptionEntity:Object{
    
    @objc dynamic var prescriptionId:Int = 0
    @objc dynamic var appointmentId:Int = 0
    @objc dynamic var title:String = ""
    @objc dynamic var date:String =  ""
    @objc dynamic var description_prescription:String = ""
    @objc dynamic var medicine:String = ""
    @objc dynamic var prescription:String = ""
    
    convenience init(presId:Int,appoId:Int,titl:String,dat:String,descr:String, medic:String, pres:String){
        self.init()
        prescriptionId = presId
        appointmentId = appoId
        title = titl
        date = dat
        description_prescription = descr
        medicine = medic
        prescription = pres
    }
    override static func primaryKey() -> String?{
        return "prescriptionId"
    }
}
