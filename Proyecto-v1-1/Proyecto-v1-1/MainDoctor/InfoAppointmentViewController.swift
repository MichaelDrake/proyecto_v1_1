//
//  InfoAppointmentViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/25/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseStorage

class InfoAppointmentViewController: UIViewController {
    var idAppointment = 0
    
    @IBOutlet weak var imagenPassient: UIImageView!
    
    
    
    @IBOutlet weak var nombrePasienteLabel: UILabel!
    
    @IBOutlet weak var genderLabel: UILabel!
    
    @IBOutlet weak var bloodType: UILabel!
    
    @IBOutlet weak var allergiesTextView: UITextView!
    
    
    @IBOutlet weak var hereditaryDiseasesTextView: UITextView!
    
    
    
    @IBOutlet weak var observationsTextView: UITextView!
    
    
    @IBOutlet weak var symptomsTextView: UITextView!
    
    
    
    @IBAction func generateButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "toGenerateSegue", sender: self)
        
    }

    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         getData()

        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    
    func getData(){
        let realm = try! Realm()
       // let userId = String(Auth.auth().currentUser!.uid)
        let currentAppointment = realm.objects(AppointmentClass.self).filter("appointmentId = \(self.idAppointment)")
        
        let infoUser = realm.objects(PassientEntity.self).filter("userId = \"\(currentAppointment[0].passientId)\"")
        
        let reference = Storage.storage().reference(withPath: "users/\(infoUser[0].userId).jpg")
        reference.getData(maxSize: (1 * 1024 * 1024)) { (data, error) in
            if let _error = error{
                print(_error)
            } else {
                if let _data  = data {
                    self.imagenPassient.image = UIImage(data: _data)
                }
            }
        }
        if (infoUser.count > 0){
            self.nombrePasienteLabel.text = infoUser[0].name
            self.genderLabel.text = infoUser[0].gender
            self.bloodType.text = infoUser[0].bloodType
            self.allergiesTextView.text = infoUser[0].allergies
            self.hereditaryDiseasesTextView.text = infoUser[0].hereditaryDiseases
        }
        else{
            print("error obteniendo InfoUser")
        }
        if (currentAppointment.count > 0){
            self.observationsTextView.text = currentAppointment[0].observations
            self.symptomsTextView.text = currentAppointment[0].symptoms
        }
        else{
            print("error obteniendo InfoAppointment")
        }
        
        
        
        
        
        
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toGenerateSegue" {
            let destination = segue.destination as! DiagnosticViewController
            destination.idAppointmentToAdd = self.idAppointment
            
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
