//
//  DiagnosticViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/25/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseAuth

class DiagnosticViewController: UIViewController {

    var idAppointmentToAdd = 0
    @IBOutlet weak var titleTextField: UITextField!
    
    
    @IBOutlet weak var medicineTextField: UITextField!
    
    
    @IBOutlet weak var prescripcionTextField: UITextField!
    
    
    @IBOutlet weak var observationsTextView: UITextView!
    
    
    
    
    @IBAction func doneButtonpressed(_ sender: Any) {
        
        sendData()

        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    func sendData(){
        if self.titleTextField.text == ""{ return}
        if self.medicineTextField.text == "" { return}
        if prescripcionTextField.text == "" { return}
        if observationsTextView.text == "" { return }
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)
        let realm = try! Realm()
        let maxValue = realm.objects(PrescriptionEntity.self).max(ofProperty: "prescriptionId") as Int?
        
        try! realm.write {
            let toEdit = realm.objects(AppointmentClass.self).filter("appointmentId = \(self.idAppointmentToAdd)")
            toEdit[0].state = "prescrito"
          
            let prescripcion = PrescriptionEntity(presId: (maxValue ?? -1) + 1, appoId: self.idAppointmentToAdd, titl: self.titleTextField.text ?? "", dat: formattedDate, descr: self.observationsTextView.text ?? "", medic: self.medicineTextField.text ?? "", pres: self.prescripcionTextField.text ?? "")
            realm.add(toEdit, update:true)
            realm.add(prescripcion, update:true)
            print(Realm.Configuration.defaultConfiguration.fileURL!)
            
            
            self.dismiss(animated: true, completion: nil)
        }
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
