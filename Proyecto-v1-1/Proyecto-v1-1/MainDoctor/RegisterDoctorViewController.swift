//
//  RegisterDoctorViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/19/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import RealmSwift

extension Date {
    static let formatte: DateFormatter = {
        let formatte = DateFormatter()
        formatte.dateFormat = "EEEE, dd MMM yyyy"
        return formatte
    }()
    var formattd: String {
        return Date.formatte.string(from: self)
    }
}

class RegisterDoctorViewController: UIViewController, UINavigationControllerDelegate,UIImagePickerControllerDelegate {

    @IBOutlet weak var txtNameDoctor: UITextField!
    
    @IBOutlet weak var txtbirthdayDoctor: UITextField!
    @IBOutlet weak var txtGenderDoctor: UITextField!
    @IBOutlet weak var txtCellphoneDoctor: UITextField!
    @IBOutlet weak var txtEspecialityDoctor: UITextField!
    @IBOutlet weak var txtUniversityDoctor: UITextField!
    @IBOutlet weak var imagePassient: UIImageView!
    let imagePickerController = UIImagePickerController()
     let datePickerView = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        imagePickerController.delegate = self
        // Do any additional setup after loading the view.
    }
    @objc func handleDatePicker(_ datePicker:UIDatePicker) {
        txtbirthdayDoctor.text = datePickerView.date.formatted
    }
    override func viewWillAppear(_ animated: Bool) {
        imagePassient.layer.cornerRadius = imagePassient.frame.height / 2.0
        imagePassient.layer.masksToBounds = true
        
        txtbirthdayDoctor.inputView = datePickerView
    }
    
    @IBAction func btnAddPicture(_ sender: Any) {
        let alertController = UIAlertController(title: "Select Source", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.imagePickerController.sourceType = .camera
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let photoAlbumAction = UIAlertAction(title: "Photo Album", style: .default) { (_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoAlbumAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnRegisterDoctor(_ sender: Any) {
        if (imagePassient.image == nil){
            self.showAlertError()
            return
        }
        
        
        
        
        let userId = String(Auth.auth().currentUser!.uid)
        
        let storage = Storage.storage()
        let usersImages = storage.reference().child("users")
        let currentUserImageRef = usersImages.child("\(userId).jpg")
        
        let userImage = imagePassient.image
        let data = userImage?.jpegData(compressionQuality: 1)
        
        let uploadTask = currentUserImageRef.putData(data!, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                return
            }
            let size = metadata.size
            currentUserImageRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    return
                }
            }
        }
        
        let db = Firestore.firestore()
        db.collection("users").document(userId).setData([
            "registerCompleted": true
        ]) { (error) in
            print(error ?? "no error")
        }
        let realm = try! Realm()
        try! realm.write {
            let doctorToStore = DoctorEntity(id: userId, nam:txtNameDoctor.text!,
                                             birt: txtbirthdayDoctor.text!, gen: txtGenderDoctor.text!, cell: txtCellphoneDoctor.text!, addrLat: "", addrLon: "", espe: txtEspecialityDoctor.text!, univ: txtUniversityDoctor.text!)
            realm.add(doctorToStore, update: true)
            print(Realm.Configuration.defaultConfiguration.fileURL!)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func showAlertError() {
        
        let alertView = UIAlertController(title: "Error", message: "Please, select an image", preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self] (_) in
            
        }
        
        alertView.addAction(okAlertAction)
        
        present(alertView, animated: true, completion: nil)
        
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("error picking image")
            return
        }
        
        imagePassient.image = image
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
