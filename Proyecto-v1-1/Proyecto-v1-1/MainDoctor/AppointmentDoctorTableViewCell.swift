//
//  AppointmentDoctorTableViewCell.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/25/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit

class AppointmentDoctorTableViewCell: UITableViewCell {

    @IBOutlet weak var imagenPaasiente: UIImageView!
    
    @IBOutlet weak var nombrePasiente: UILabel!
    
    @IBOutlet weak var fechaAppointment: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
