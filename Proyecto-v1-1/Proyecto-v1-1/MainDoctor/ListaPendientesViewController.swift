//
//  ListaPendientesViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/4/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//
import UIKit
import RealmSwift
import FirebaseAuth
import FirebaseStorage

class ListaPendientesViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var appointmentsTableView: UITableView!
    
    var appointments = [AppointmentClass]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        appointments.removeAll()
        getData()
        self.appointmentsTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointments.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentDoctorCell") as! AppointmentDoctorTableViewCell
        let idPasiente = appointments[indexPath.row].passientId
        
        
        let reference = Storage.storage().reference(withPath: "users/\(idPasiente).jpg")
        reference.getData(maxSize: (1 * 1024 * 1024)) { (data, error) in
            if let _error = error{
                print(_error)
            } else {
                if let _data  = data {
                    cell.imagenPaasiente.image = UIImage(data: _data)
                }
            }
        }
        let realm = try! Realm()
        let infoPassient = realm.objects(PassientEntity.self).filter("userId = \"\(idPasiente)\"")
        cell.nombrePasiente.text = infoPassient[0].name
        cell.fechaAppointment.text = appointments[indexPath.row].date
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toInfoAppointmentSegue", sender: self)
    }
    
    
    
    func getData(){
        let realm = try! Realm()
        let userId = String(Auth.auth().currentUser!.uid)
        
        let gotAppointments = realm.objects(AppointmentClass.self).filter("doctorId = \"\(userId)\"")
        appointments.append(contentsOf: gotAppointments)
        
        self.appointmentsTableView.reloadData()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toInfoAppointmentSegue" {
            let destination = segue.destination as! InfoAppointmentViewController
            let selected = appointments[appointmentsTableView.indexPathForSelectedRow?.row ?? 0]
            destination.idAppointment = selected.appointmentId
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
