//
//  CreateAccountViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 6/28/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import FirebaseAuth
import RealmSwift



class CreateAccountViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource{
    
    
    let typeUsers = ["Passient","Doctor"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return typeUsers.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
     
        return typeUsers[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        userTypeTextField.text = typeUsers[row]
    }

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var userTypeTextField: UITextField!
    
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createButtonPressed(_ sender: Any) {
        
        
        Auth.auth().createUser(
            
            withEmail: emailTextField.text!,
            password: passwordTextField.text!) { (result, error) in
                
                if let _ = error {
                    self.showErrorAlert(withMessage: error?.localizedDescription ?? "Error")
                } else {
                    let userId = String(Auth.auth().currentUser!.uid)
                    print("EL USUARIO EEEEESSSSSS")
                    print(userId)
                    let realm = try! Realm()
                    try! realm.write{
                    
                             let UserStored = UserClass(id:userId,ml:self.emailTextField.text ?? "",ps:self.passwordTextField.text ?? "",ty:self.userTypeTextField.text ?? "")
                            realm.add(UserStored, update:true)
                            print(Realm.Configuration.defaultConfiguration.fileURL!)
                        self.performSegue(withIdentifier: "toLoginSegue", sender: self)
                        
                    }
                    
                    
                    
                    
                   // if(self.userTypeTextField.text == "Doctor"){
                     //   let mainViewController = self.storyboard?.instantiateViewController(withIdentifier: "UserDoctorInfoViewController") as! UserInfoViewController
                      //  self.present(mainViewController, animated: true, completion: nil)
                        
                   // }else{
                    //    let mainViewController = self.storyboard?.instantiateViewController(withIdentifier: "UserPassientInfoViewController") //as! UserInfoViewController
                    //    self.present(mainViewController, animated: true, completion: nil)
                        
                   // }
                    
                    	
                    
                    
                }
        }
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
            confirmPasswordTextField.delegate = self
       

    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if passwordTextField.text! != confirmPasswordTextField.text {
            showErrorAlert(withMessage: "Passwords don't match, please retry it")
        }
    }
    func showErrorAlert(withMessage message:String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    

    

    

}
