//
//  MyAppointmentsViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/20/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseAuth

class MyAppointmentsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var appointmentsTableView: UITableView!
    var appointments = [AppointmentClass]()

    override func viewDidLoad() {
        super.viewDidLoad()
        getData()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        appointments.removeAll()
        getData()
        self.appointmentsTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointments.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentCell") as! AppointmentTableViewCell
        
        cell.observationLabel.text = appointments[indexPath.row].observations
        cell.dateLabel.text = appointments[indexPath.row].date
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toInfoAppointmentSegue", sender: self)
    }

    
    
    func getData(){
        let realm = try! Realm()
         let userId = String(Auth.auth().currentUser!.uid)
        
        let gotAppointments = realm.objects(AppointmentClass.self).filter("passientId = \"\(userId)\"")
        appointments.append(contentsOf: gotAppointments)
        
        self.appointmentsTableView.reloadData()
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toInfoAppointmentSegue" {
            let destination = segue.destination as! AppointmentDetailsViewController
            let selected = appointments[appointmentsTableView.indexPathForSelectedRow?.row ?? 0]
            destination.appointmentId = selected.appointmentId
            
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
