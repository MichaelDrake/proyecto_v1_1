//
//  RecoverPasswordViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/13/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import FirebaseAuth

class RecoverPasswordViewController: UIViewController {

    @IBOutlet weak var emailText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func DoneRecoverButton(_ sender: Any) {
        Auth.auth().sendPasswordReset(withEmail: self.emailText.text!) { error in
        }
        
        let alert = UIAlertController(title: "CAMBIAR CONTRASEÑA", message: "VERIFIQUE SU EMAIL PARA CAMBIAR CONTRASEÑA", preferredStyle: .alert)
        let subButton = UIAlertAction(title: "OK", style: .default , handler: nil)
        
        alert.addAction(subButton)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func CancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
