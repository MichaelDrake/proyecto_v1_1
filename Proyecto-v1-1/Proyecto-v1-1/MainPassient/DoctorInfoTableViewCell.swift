//
//  DoctorInfoTableViewCell.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/12/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
protocol DoctorSelecterCellDelegate{
    func selectedDoctor(idSelected:String)
    
}

class DoctorInfoTableViewCell: UITableViewCell {
    
    var delegate: DoctorSelecterCellDelegate?
    var idDoctor: String?
	
    @IBOutlet weak var imageDoctor: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var especialidadLabel: UILabel!
    
    @IBOutlet weak var cellphoneLabel: UILabel!
    
    
    @IBAction func selectButtonPressed(_ sender: Any) {
        
        
        delegate?.selectedDoctor(idSelected: idDoctor ?? "")
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
