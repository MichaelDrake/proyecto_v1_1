//
//  ViewInfoDoctorViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/20/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseAuth
import FirebaseStorage

class ViewInfoDoctorViewController: UIViewController {
    var userDoctorId:String = ""
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var cellphoneLabel: UILabel!
    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var univerityLabel: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        
        // Do any additional setup after loading the view.
    }
    
    
    func getData(){
        if(self.userDoctorId == ""){ return}
        let realm = try! Realm()
        
        let reference = Storage.storage().reference(withPath: "users/\(self.userDoctorId).jpg")
        reference.getData(maxSize: (1 * 1024 * 1024)) { (data, error) in
            if let _error = error{
                print(_error)
            } else {
                if let _data  = data {
                    self.userImageView.image = UIImage(data: _data)
                }
            }
        }
        let infoUser = realm.objects(DoctorEntity.self).filter("userId = \"\(self.userDoctorId)\"")
        
        
        if (infoUser.count > 0){
            self.nameLabel.text = infoUser[0].name
            self.genderLabel.text = infoUser[0].gender
            
            self.cellphoneLabel.text = infoUser[0].cellphone
            self.birthdayLabel.text = infoUser[0].birthday
            self.specialityLabel.text = infoUser[0].especiality
            self.univerityLabel.text = infoUser[0].university
        }
        else{
            print("error obteniendo InfoUser")
            
        }
    }

}
