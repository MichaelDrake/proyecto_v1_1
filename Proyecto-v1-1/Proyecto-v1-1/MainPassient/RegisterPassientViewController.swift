//
//  RegisterPassientViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/6/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import RealmSwift

extension Date {
    static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd MMM yyyy"
        return formatter
    }()
    var formatted: String {
        return Date.formatter.string(from: self)
    }
}


class RegisterPassientViewController: UIViewController, UINavigationControllerDelegate,UIImagePickerControllerDelegate{

    @IBOutlet weak var imagePassient: UIImageView!
    

    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var birthDayTextField: UITextField!
    
    
    
    @IBOutlet weak var genderTextField: UITextField!
    
    @IBOutlet weak var bloodTypeTextField: UITextField!
    
    @IBOutlet weak var allergiesTextField: UITextView!
    
    @IBOutlet weak var hereditaryDiseasesTextField: UITextView!
    
    
    let datePickerView = UIDatePicker()
    
    let imagePickerController = UIImagePickerController()
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        imagePickerController.delegate = self
        // Do any additional setup after loading the view.
    }
    @objc func handleDatePicker(_ datePicker:UIDatePicker) {
        birthDayTextField.text = datePickerView.date.formatted
    }
    override func viewWillAppear(_ animated: Bool) {
        imagePassient.layer.cornerRadius = imagePassient.frame.height / 2.0
        imagePassient.layer.masksToBounds = true
        
        birthDayTextField.inputView = datePickerView
    }
    
    
    @IBAction func addPictureButtonPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "Select Source", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.imagePickerController.sourceType = .camera
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let photoAlbumAction = UIAlertAction(title: "Photo Album", style: .default) { (_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoAlbumAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        
        if (imagePassient.image == nil){
            self.showAlertError()
            return
        }
        
        
        
        
        let userId = String(Auth.auth().currentUser!.uid)
        
        let storage = Storage.storage()
        let usersImages = storage.reference().child("users")
        let currentUserImageRef = usersImages.child("\(userId).jpg")
        
        let userImage = imagePassient.image
        let data = userImage?.jpegData(compressionQuality: 1)
        
        let uploadTask = currentUserImageRef.putData(data!, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                return
            }
            let size = metadata.size
            currentUserImageRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    return
                }
            }
        }
        
        let db = Firestore.firestore()
        db.collection("users").document(userId).setData([
            "name" : nameTextField.text ?? "",
            "gender" : genderTextField.text ?? "",
            "birthdate": datePickerView.date,
            "bloodType": bloodTypeTextField.text ?? "",
            "allergies": allergiesTextField.text ?? "",
            "hereditaryDiseases": hereditaryDiseasesTextField.text ?? "",
            "registerCompleted": true
        ]) { (error) in
            print(error ?? "no error")
        }
        let realm = try! Realm()
        try! realm.write {
            let passientToStore = PassientEntity(id:userId,nam: self.nameTextField.text ?? "", brd: self.birthDayTextField.text ?? "",gen: self.genderTextField.text ?? "",blt: self.bloodTypeTextField.text ?? "", all: self.allergiesTextField.text ?? "" ,hdi: self.hereditaryDiseasesTextField.text ?? "" )
            realm.add(passientToStore, update:true)
            print(Realm.Configuration.defaultConfiguration.fileURL!)
            
        
            self.dismiss(animated: true, completion: nil)
        }
        
        
        
    }
    
    
    
    func showAlertError() {
        
        let alertView = UIAlertController(title: "Error", message: "Please, select an image", preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self] (_) in
           
        }
        
        alertView.addAction(okAlertAction)
        
        present(alertView, animated: true, completion: nil)
        
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("error picking image")
            return
        }
        
        imagePassient.image = image
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
