//
//  CreateAppointmentViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/5/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import FirebaseStorage
import RealmSwift
import FirebaseAuth

class CreateAppointmentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, recivirIdDoctorDelegate{
  
    
    
    var idDoctorMostrar:String?
    var symptoms = [String]()

    
    
    
    @IBOutlet weak var symptomsTableview: UITableView!
    
    @IBOutlet weak var observationsTextField: UITextView!
    var symptomEnteredTextFiel:UITextField?
    
    @IBOutlet weak var doctorSelectedLabel: UILabel!
    
    @IBOutlet weak var imagenDoctorSelected: UIImageView!
    
    
    @IBAction func addButtonPressed(_ sender: Any) {
        
      //  if(symptomTextField.text == ""){
       //     return
       // }
     //   symptoms.append(symptomTextField.text ?? "")
        
    //
        
        let alertController = UIAlertController(title: "", message: "Enter a Symptom", preferredStyle: .alert)
        alertController.addTextField(configurationHandler: symptomEnteredTextField)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: self.okHandler)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true)
        
        
        
    }
    func accionConId(idDoctorRecivido: String) {
        self.idDoctorMostrar = idDoctorRecivido
        
        let reference = Storage.storage().reference(withPath: "users/\(idDoctorRecivido).jpg")
        reference.getData(maxSize: (1 * 1024 * 1024)) { (data, error) in
            if let _error = error{
                print(_error)
            } else {
                if let _data  = data {
                    self.imagenDoctorSelected.image = UIImage(data: _data)
                }
            }
        }
        let realm = try! Realm()
        let user = realm.objects(DoctorEntity.self).filter("userId = \"\(idDoctorRecivido)\"")
        
        self.doctorSelectedLabel.text = user[0].name
        
    }
    
    func symptomEnteredTextField(textField: UITextField!){
        symptomEnteredTextFiel = textField
        textField.placeholder = "Any symptom"
        
    }
    
    func okHandler(alert: UIAlertAction!){

        symptoms.insert(symptomEnteredTextFiel?.text ?? "", at: 0)
        self.symptomsTableview.reloadData()
    }
    
    
    @IBAction func selectDoctorButtonPressed(_ sender: Any) {
        
        self.performSegue(withIdentifier: "toSelectDoctorSegue", sender: self)
        
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        if(self.symptoms.count <= 0){return}
        if(self.observationsTextField.text.isEmpty){return}
        if(self.idDoctorMostrar == nil){return}
        let userId = String(Auth.auth().currentUser!.uid)
        var symtpmsToStore:String = ""
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)
        
        for symptom in symptoms
        {
            
            symtpmsToStore += "\(symptom)\n"
        }
        
        let realm = try! Realm()
        let maxValue = realm.objects(AppointmentClass.self).max(ofProperty: "appointmentId") as Int?
        try! realm.write {
            let appointmentToStore = AppointmentClass(appo:(maxValue ?? -1) + 1,pass: userId , doct: self.idDoctorMostrar ?? "",symp: symtpmsToStore, obse: self.observationsTextField.text ?? "" , dat: formattedDate, stat: "pendiente")
            realm.add(appointmentToStore, update:true)
            print(Realm.Configuration.defaultConfiguration.fileURL!)
            cleanFields()

        }
        
        
        
        
        
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
    }
    
    func cleanFields(){
        self.symptoms.removeAll()
        self.symptomsTableview.reloadData()
        self.imagenDoctorSelected.image = nil
        self.doctorSelectedLabel.text = ""
        self.observationsTextField.text = ""
    
    }
    
    
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return symptoms.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SymptomCell") as! SymptomTableViewCell
        
        cell.symptomLabel.text = symptoms[indexPath.row]
    
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toSelectDoctorSegue" {
            let destination = segue.destination as! SelectDoctorViewController

            destination.delegate = self
            
    
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
