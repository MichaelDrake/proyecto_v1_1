//
//  AppointmentDetailsViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/20/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseStorage
import FirebaseAuth

class AppointmentDetailsViewController: UIViewController {
    
    var appointmentId:Int = 0
    
    
    
    @IBOutlet weak var doctorNameLabel: UILabel!
    
    @IBOutlet weak var appointmentSymptomsTextView: UITextView!
    @IBOutlet weak var appointmentObservationsTextView: UITextView!
    @IBOutlet weak var imagenDoctor: UIImageView!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var fechaLabel: UILabel!
    @IBOutlet weak var descripcionTextView: UITextView!
    @IBOutlet weak var medicamentoLabel: UILabel!
    @IBOutlet weak var perscripcionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()

        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    func getData(){
        let realm = try! Realm()
        
        let appointmentShow = realm.objects(AppointmentClass.self).filter("appointmentId = \(self.appointmentId)")
        let doctorId:String = appointmentShow[0].doctorId
        
        let reference = Storage.storage().reference(withPath: "users/\(doctorId).jpg")
        reference.getData(maxSize: (1 * 1024 * 1024)) { (data, error) in
            if let _error = error{
                print(_error)
            } else {
                if let _data  = data {
                    self.imagenDoctor.image = UIImage(data: _data)
                }
            }
        }
        self.appointmentSymptomsTextView.text = appointmentShow[0].symptoms
        self.appointmentObservationsTextView.text = appointmentShow[0].observations
        
        let doctorShow = realm.objects(DoctorEntity.self).filter("userId = \"\(doctorId)\"")
        
        self.doctorNameLabel.text = "Doctor \(doctorShow[0].name) said:"
        
        
        
        if(appointmentShow[0].state == "pendiente"){
            return
            
        }else{
            
            let prescriptionShow = realm.objects(PrescriptionEntity.self).filter("appointmentId == \(appointmentShow[0].appointmentId)")
            
            if (prescriptionShow.count > 0){
                
                self.tituloLabel.text = prescriptionShow[0].title
                self.fechaLabel.text = prescriptionShow[0].date
                self.descripcionTextView.text = prescriptionShow[0].description_prescription
                self.medicamentoLabel.text = "Medicamento: \(prescriptionShow[0].medicine)"
                self.perscripcionLabel.text = "Tomar el medicamento: \(prescriptionShow[0].prescription)"
                
            }
            
        }
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
