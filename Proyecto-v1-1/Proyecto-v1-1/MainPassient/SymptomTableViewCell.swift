//
//  SymptomTableViewCell.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/9/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit

class SymptomTableViewCell: UITableViewCell {

    @IBOutlet weak var symptomLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
