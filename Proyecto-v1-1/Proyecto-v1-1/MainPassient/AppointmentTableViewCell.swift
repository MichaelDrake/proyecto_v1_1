//
//  AppointmentTableViewCell.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/20/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit

class AppointmentTableViewCell: UITableViewCell {



    @IBOutlet weak var observationLabel: UILabel!
    

    @IBOutlet weak var dateLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
