//
//  SelectDoctorViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 7/12/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseStorage
import FirebaseAuth
protocol recivirIdDoctorDelegate{
    func accionConId(idDoctorRecivido:String)
}

class SelectDoctorViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, DoctorSelecterCellDelegate{
    var delegate: recivirIdDoctorDelegate?
    
     let realm = try! Realm()
    
     @IBOutlet weak var doctoresTableView: UITableView!
      var doctores = [DoctorEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDoctors()
        
        // Do any additional setup after loading the view.
    }
    func selectedDoctor(idSelected: String) {
        self.delegate?.accionConId(idDoctorRecivido: idSelected)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return doctores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorInfoTableViewCell") as! DoctorInfoTableViewCell
    
        let reference = Storage.storage().reference(withPath: "users/\(doctores[indexPath.row].userId).jpg")
        reference.getData(maxSize: (1 * 1024 * 1024)) { (data, error) in
            if let _error = error{
                print(_error)
            } else {
                if let _data  = data {
                    cell.imageDoctor.image = UIImage(data: _data)
                }
            }
        }
    
       let realm = try! Realm()
        let user = realm.objects(UserClass.self).filter("userId = \"\(doctores[indexPath.row].userId)\"")
            cell.nameLabel.text = doctores[indexPath.row].name
            cell.emailLabel.text = user[0].email
            cell.especialidadLabel.text = doctores[indexPath.row].especiality
            cell.cellphoneLabel.text = doctores[indexPath.row].cellphone
            cell.idDoctor = doctores[indexPath.row].userId
            cell.delegate = self
     
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toInfoDoctorSegue", sender: self)
    }

    
    
    
    
    
    
    
    
    
    
    
    
    func getDoctors() {
        
        let gotDoctors = realm.objects(DoctorEntity.self)
        doctores.append(contentsOf: gotDoctors)
        
        self.doctoresTableView.reloadData()
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toInfoDoctorSegue" {
            let destination = segue.destination as! ViewInfoDoctorViewController
            let selected = doctores[doctoresTableView.indexPathForSelectedRow?.row ?? 0]
            destination.userDoctorId = selected.userId
            
            
        }
    }
    //toInfoDoctorSegue
    
   

    



}
