//
//  ViewController.swift
//  Proyecto-v1-1
//
//  Created by Michael Morales IOs on 6/8/19.
//  Copyright © 2019 Michael Morales IOs. All rights reserved.
//

import UIKit
import FirebaseAuth
import RealmSwift
import FirebaseFirestore
class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
   
    @IBAction func createAccountButtonPressed(_ sender: Any) {
         self.performSegue(withIdentifier: "toCreateAccountSegue", sender: self)
    }
    
    
    
    @IBAction func loginButtonPressed(_ sender: Any) {
   
    
    
    
    
    firebaseAuth(email: emailTextField.text!, passwd: passwordTextField.text!)
    
    
    
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // let realm = try! Realm()
        //let items = realm.objects(UserEntity.self)
        //try! realm.write {
          //  realm.deleteAll()
       // }
    }

    
    
    
    
    
    
    func firebaseAuth(email:String, passwd:String) {
        
        Auth.auth().signIn(withEmail: email, password: passwd) { (result, error) in
            
            if let _ = error {
                self.showAlertError()
                return
            }
            
            
            let realm = try! Realm()
            
            
            let user = realm.objects(UserClass.self).filter("email = \"\(email)\"")
            
            
            
            
            
            
            self.performSegue(withIdentifier: "toRegister\(user[0].type)Segue", sender: self)
            
            
            
            
            
            
            
            
            
            
            
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let db = Firestore.firestore()
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        
        let userRef = db.collection("users").document(currentUser.uid)
        let userId = String(currentUser.uid)
        let realm = try! Realm()
        let user = realm.objects(UserClass.self).filter("userId = \"\(userId)\"")
        
        
        
        
        
        
        userRef.getDocument { (snapshot, error) in
            self.activityIndicator.isHidden = true
            if error != nil || snapshot?.get("registerCompleted") == nil  {
                self.performSegue(withIdentifier: "toRegister\(user[0].type)Segue", sender: self)
            } else {
                self.performSegue(withIdentifier: "to\(user[0].type)MainSegue", sender: self)
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    func showAlertError() {
        
        let alertView = UIAlertController(title: "Error", message: "Please enter valid credentials", preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self] (_) in
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
        }
        
        alertView.addAction(okAlertAction)
        
        present(alertView, animated: true, completion: nil)
        
    }
    
    
    
    
    

}

